import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AngularFireModule} from '@angular/fire';
import { AngularFireDatabaseModule } from "@angular/fire/database";
import {AngularFireStorageModule} from "@angular/fire/storage"

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ListComponent } from './list/list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CreateComponent } from './create/create.component';
import { CrudService } from './services/crud.service';
import { environment } from 'src/environments/environment';
import { AgmCoreModule } from '@agm/core';
import { MapsComponent } from './maps/maps.component';
import { UpdateComponent } from './update/update.component'
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ListComponent,
    CreateComponent,
    MapsComponent,
    UpdateComponent,

  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD7iDhQu-Xt2weM4wGQFNCvYg3zGhsGLZ4'
    })
  ],
  providers: [CrudService],
  bootstrap: [AppComponent]
})
export class AppModule { }
