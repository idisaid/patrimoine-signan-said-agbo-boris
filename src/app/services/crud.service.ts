import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from "@angular/fire/firestore"
import { Bien } from '../bien.model';
@Injectable({
  providedIn: 'root'
})
export class CrudService {
  private dbPath = '/biens';

  reference: AngularFirestoreCollection<Bien>;

  constructor(private db: AngularFirestore) {
    this.reference = db.collection(this.dbPath);
  }

  createGood(bien: Bien): void {
    this.reference.add({...bien});

  }

  updateGood(id: any, data: any): Promise<void> {
    return this.reference.doc(id).update(data);
  }

  deleteGood(id: string): Promise<void> {
    return this.reference.doc(id).delete();
  }

  getGood(id: any) {
    return this.db
    .collection(this.dbPath)
    .doc(id)
    .valueChanges()
  }
  getGoodList(): AngularFirestoreCollection<Bien> {
    return this.reference;
  }

  deleteAll() {
    this.reference.get().subscribe(
      querySnapshot => {
        querySnapshot.forEach((doc) => {
          doc.ref.delete();
        });
      },
      error => {
        console.log('Error: ', error);
      });
  }

}
