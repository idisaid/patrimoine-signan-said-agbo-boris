import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { ListComponent } from './list/list.component';
import { LoginComponent } from './login/login.component';
import { MapsComponent } from './maps/maps.component';
import { UpdateComponent } from './update/update.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'list', component: ListComponent},
  {path: 'maps/:id', component: MapsComponent},
  {path: 'update/:id', component: UpdateComponent},
  {path: 'create', component: CreateComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
