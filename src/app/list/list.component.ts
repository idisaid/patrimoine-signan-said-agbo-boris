import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CrudService } from '../services/crud.service';
import { map } from 'rxjs/operators';
import { Bien } from '../bien.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {


  @Input()good!: Bien;
  bien: any;

  constructor(private crudservice: CrudService) { }

  ngOnInit() { 
    this.getGoodList();
  }

  getGoodList() {
    this.crudservice.getGoodList().snapshotChanges().pipe(
      map((changes: any[]) =>
        changes.map(c =>
          ({ id: c.payload.doc.id, ...c.payload.doc.data() })
        )
      )
    ).subscribe(lists => {
      this.bien = lists;
    });
  }

  deleteGoods() {
    this.crudservice.deleteAll();
  
  }

  deleteGood(id: any) {
    this.crudservice
      .deleteGood(id)
      .catch(err => console.log(err));
  }
 
}
