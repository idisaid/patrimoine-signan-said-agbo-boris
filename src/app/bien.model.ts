import { Timestamp } from "rxjs";

export class Bien {

    dateAjout= new Date().toDateString();
    id!: string;
    nom!: string;
    ville!: string;
    latitude!: string;
    longitude!: string;
}
