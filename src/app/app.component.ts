import { Component } from '@angular/core';
import * as firebase from 'firebase';
import "firebase/auth";
import "firebase/firestore";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'examen-angular';
  constructor(){
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyD7iDhQu-Xt2weM4wGQFNCvYg3zGhsGLZ4",
  authDomain: "patrimoine-3d546.firebaseapp.com",
  databaseURL: "https://patrimoine-3d546-default-rtdb.firebaseio.com",
  projectId: "patrimoine-3d546",
  storageBucket: "patrimoine-3d546.appspot.com",
  messagingSenderId: "44032078498",
  appId: "1:44032078498:web:0fecc8a7f3dbb95a545dfb",
  measurementId: "G-H8D96ETQQP"
};
      firebase.default.initializeApp(firebaseConfig);
  }
}
