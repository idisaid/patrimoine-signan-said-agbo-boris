import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService } from '../services/crud.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  public editGood: FormGroup;
  goodRef: any

  constructor(
    public crudservice: CrudService,
    public formBuilder: FormBuilder,
    private act: ActivatedRoute,
    private router: Router
  ) {
    this.editGood = this.formBuilder.group({
      nom: [''],
      ville: [''],
      latitude: [''],
      longitude: ['']

    })
  }

  ngOnInit(): void {
    const id = this.act.snapshot.paramMap.get('id');

    this.crudservice.getGood(id).subscribe(res => {
      this.goodRef = res;
      this.editGood = this.formBuilder.group({
        nom: [this.goodRef.nom],
        ville: [this.goodRef.ville],
        latitude: [this.goodRef.latitude],
        longitude: [this.goodRef.latitude]
      })      
    })
  }

  onSubmit() {
    const id = this.act.snapshot.paramMap.get('id');
    
    this.crudservice.updateGood(id, this.editGood.value);
    this.router.navigate(['list']);
  };

}
