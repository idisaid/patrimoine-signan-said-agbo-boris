import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthServiceService } from '../services/auth-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  signupForm!: FormGroup;
  errorMessage!: String;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthServiceService,
              private router: Router) { }



  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
    });
  }

  onSubmit() {
    const email = this.signupForm?.get('email')?.value;
    const password = this.signupForm?.get('password')?.value;
    
    this.authService.signInUser(email, password).then(
      () => {
        this.router.navigate(['/list']);
      },
      (error) => {
        this.errorMessage = error;
      }
    );
  }

}
