import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Bien } from '../bien.model';
import { CrudService } from '../services/crud.service';
import { FormsModule } from '@angular/forms'
@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  bien: Bien = new Bien();


  constructor(public crudservice: CrudService) { 
    
  }

  ngOnInit() {
  }

  goodForm = new FormGroup({
    nom: new FormControl(),
    ville: new FormControl(),
    latitude: new FormControl(),
    longitude: new FormControl(),
    dateAjout: new FormControl()
});

save() {
  this.crudservice.createGood(this.bien);
  this.bien = new Bien();
}

onFormSubmit(): void {
  console.log('Name:' + this.goodForm.get('nom')?.value);
  this.save();

}


}
