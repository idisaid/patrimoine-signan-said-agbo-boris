import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { CrudService } from '../services/crud.service';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit {
  bien: any;
  lat: any;
  lng: any;
  public editGood: FormGroup;
  goodRef: any

  constructor(
    public crudservice: CrudService,
    public formBuilder: FormBuilder,
    private act: ActivatedRoute,
    private router: Router
  ) {
    this.editGood = this.formBuilder.group({
      nom: [''],
      ville: [''],
      latitude: [''],
      longitude: ['']

    })
  }

  ngOnInit(): void {
    const id = this.act.snapshot.paramMap.get('id');

    this.crudservice.getGood(id).subscribe(res => {
      this.goodRef = res;
      this.lat = this.goodRef.latitude;
      this.lng = this.goodRef.longitude;
   console.log( this.lat + " " + this.lng);
    })
  }
}
